var $ = require('jquery');
window.$ = window.jQuery = $;

var magnificPopup = require('magnific-popup');
var jcarousel = require('jcarousel');

$('.js-popup').magnificPopup({
  type: 'inline',
  preloader: false,
  focus: '#ContactForm_name',

  callbacks: {
    beforeOpen: function() {
      if($(window).width() < 700) {
        this.st.focus = false;
      } else {
        this.st.focus = '#ContactForm_name';
      }
    }
  }
});

$('.js-certificates').magnificPopup({
  delegate: 'a', // child items selector, by clicking on it popup will open
   gallery: {
      enabled: true
    },
  type: 'image'
});


$('.js-reviews').jcarousel({wrap:'circular'});

$('.js-reviews__ctrlPrev')
          .on('jcarouselcontrol:active', function() {
              $(this).removeClass('-inactive');
          })
          .on('jcarouselcontrol:inactive', function() {
              $(this).addClass('-inactive');
          })
          .jcarouselControl({
              target: '-=1'
          });

$('.js-reviews__ctrlNext')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('-inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('-inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });

$('.js-reviews__pagination')
            .on('jcarouselpagination:active', 'li', function() {
                $(this).addClass('-active');
            })
            .on('jcarouselpagination:inactive', 'li', function() {
                $(this).removeClass('-active');
            })
            .jcarouselPagination({
                'item': function(page, carouselItems) {
                return '<li class="js-reviews__itemCtrl"><a href="#' + page + '"><i class="b-icon -circle"></i></a></li>';
              }
            });


$('.js-form').submit(function(event){
  var data = $(this).serialize();
  var name = $(this).find('input[name=name]').val();
  var phone = $(this).find('input[name=phone]').val();

  event = event || window.event;
  event.preventDefault();

  $(this).attr('disabled','disabled');

  $.post("/", data).done(function(msg){

    var orderID = 0;

    if(msg)
      orderID = msg;

    $('#contact-form-div').addClass('lead-complete');
    $('#contact-form-div').html('Заявка отправленна!<br/>Наши менеджеры свяжуться с вами в ближайшее время...');

    $.magnificPopup.open({
      items: {
        src: '#js-contactForm',
        type: 'inline'
      }
    });


  })
  .fail(function(msg){
    $('#contact-form-div').addClass('lead-complete');

    $.magnificPopup.open({
      items: {
        src: '#js-contactForm',
        type: 'inline'
      }
    });

    if(msg.responseText) {
      $('#contact-form-div').text(msg.responseText);
    } else {
      $('#contact-form-div').text("Ошибка валидации");
    }

  });

});







$('.js-form-modal').submit(function(event){
  var data = $(this).serialize();
  var name = $(this).find('input[name=name]').val();
  var phone = $(this).find('input[name=phone]').val();

  event = event || window.event;
  event.preventDefault();

  $(this).find('input[type=submit]').attr('value', 'Заявка отправляется...');
  $(this).attr('disabled','disabled');

  $.post("/", data).done(function(msg){

    var orderID = 0;

    if(msg)
      orderID = msg;

    $('#contact-form-div').addClass('lead-complete');
    $('#contact-form-div').html('Заявка отправленна!<br/>Наши менеджеры свяжуться с вами в ближайшее время...');


  })
  .fail(function(msg){
    $('#contact-form-div').addClass('lead-complete');

    if(msg.responseText) {
      $('#contact-form-div').text(msg.responseText);
    } else {
      $('#contact-form-div').text("Ошибка валидации");
    }

  });

});
