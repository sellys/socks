var config = require('./')

module.exports = {
  src: config.sourceAssets + "/sprites/*.png",
  tmp: './src/stylesheets/sprites/*.png',
  dest: config.publicAssets + '/css'
}
