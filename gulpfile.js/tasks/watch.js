var gulp      = require('gulp');
var html      = require('../config/html');
var jade      = require('../config/jade');
var sprite    = require('../config/spritesmith');
var images    = require('../config/images');
var stylus    = require('../config/stylus');
var fonts     = require('../config/fonts');
var watch     = require('gulp-watch');

gulp.task('watch', ['browserSync'], function() {
  watch(images.src, function() { gulp.start('images'); });
  watch(stylus.src, function() { gulp.start('stylus'); });
  watch(sprite.tmp, function() { gulp.start('move-sprite'); });
  watch(fonts.src, function() { gulp.start('fonts'); });
  // watch(html.watch, function() { gulp.start('html'); });
  watch(jade.watch, function() { gulp.start('jade'); });
});
