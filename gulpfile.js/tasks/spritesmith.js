var config      = require('../config/spritesmith');
var gulp        = require('gulp');
var browserSync = require('browser-sync');
var spritesmith = require('gulp.spritesmith');
var imagemin    = require('gulp-imagemin');
var pngquant    = require('imagemin-pngquant');

gulp.task('sprite', function () {
  return gulp.src(config.src)
    .pipe(spritesmith({
      imgName: 'sprite.png',
      cssName: '_sprite.styl'
    }))
    .pipe(imagemin({
      use: [pngquant()]
    }))
    .pipe(gulp.dest('./src/stylesheets/sprites/'));
});

gulp.task('move-sprite', function () {
  return gulp.src(config.tmp)
    .pipe(gulp.dest(config.dest));
});
